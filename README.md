<div align="center">
  <img src="https://gitlab.com/xkrishguptaa/learning-rust-book/-/raw/main/assets/logo.png" height="100px" width="100px" />
  <br />
  <h1>Learning Rust Book</h1>
  <p>Learning rust by following the book <a href="https://doc.rust-lang.org/book/">The Rust Programming Language</a></p>
</div>

## 📖 Chapters

| Chapter | Name | Status |
| ------- | ---- | ------ |
| 1 | Getting Started | ✅ |
| 1.1 | Installation | ✅ |
| 1.2 | [Hello, World!](https://gitlab.com/xkrishguptaa/learning-rust-book/-/tree/main/1.2-hello-world) | ✅ |
| 1.3 | [Hello, Cargo!](https://gitlab.com/xkrishguptaa/learning-rust-book/-/tree/main/1.3-hello-cargo) | ✅ |
| 2 | [Programming a Guessing Game](https://gitlab.com/xkrishguptaa/learning-rust-book/-/tree/main/2-guessing-game) | ✅ |
